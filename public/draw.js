const canvas = document.querySelector(".canvas");
const scoreB = document.querySelector(".score");
const ctx = canvas.getContext("2d");
const scb = scoreB.getContext("2d");
const scale = 20;
const rows = canvas.height / scale;
const columns = canvas.width/ scale;
var snake;



let dead = new Audio();
let eat = new Audio();
let up = new Audio();
let right = new Audio();
let left = new Audio();
let down = new Audio();

dead.src = "audio/dead.mp3";
eat.src = "audio/eat.mp3";
up.src = "audio/up.mp3";
right.src = "audio/right.mp3";
left.src = "audio/left.mp3";
down.src = "audio/down.mp3";

var isPaused = false;




(function setup() {
  snake = new Snake();
  fruit = new Fruit();
  fruit.pickLocation();

  window.setInterval(() => {

    if(!isPaused) {
      ctx.clearRect(0,0, canvas.width, canvas.height);
      scb.clearRect(0,0, canvas.width, canvas.height);
      fruit.draw();
      snake.update();
      snake.draw();

      if (snake.eat(fruit)) {
        fruit.pickLocation();
      }

      isPaused = snake.checkCollision();

      scb.fillStyle = "white";
      
      if(!isPaused) {
        scb.font = "bold 40px Courier New";
        scb.fillText("Score : "+snake.total,15,40);
      } else {
        scb.font = "bold 50px Courier New";
        scb.fillText("GAME OVER",115,30);
        
        scb.font = "20px Courier New";
        scb.fillText("Press Enter to Restart",115,55); 
      }

    }

  }, 200);

  
}());
window.addEventListener("keyup", function(e) {
  if(!isPaused) return
  if (event.keyCode === 13) {
    e.preventDefault();
    isPaused = false;
  }
});


window.addEventListener('keydown', ((evt) => {
  const direction = evt.key.replace('Arrow', '');
  snake.changeDirection(direction);
}));
