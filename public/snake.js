function Snake() {
  this.x = 0;
  this.y = 0;
  this.xSpeed = scale * 1;
  this.ySpeed = 0;
  this.total = 0;
  this.tail = [];

  this.draw = function() {
    ctx.fillStyle = "#33F9FF";
    for (let i=0; i<this.tail.length; i++) {
      ctx.fillRect(this.tail[i].x,this.tail[i].y, scale, scale);
    }

    ctx.fillRect(this.x, this.y, scale, scale);
  }

  this.update = function() {
    for (let i=0; i<this.tail.length - 1; i++) {
      this.tail[i] = this.tail[i+1];
    }

    this.tail[this.total - 1] =
      { x: this.x, y: this.y };

    this.x += this.xSpeed;
    this.y += this.ySpeed;
//ta-ru scale -> 0 canvas.width-scale -> canvas.width
    if (this.x > canvas.width-scale) {
      this.x = 0;
    }

    if (this.y > canvas.height-scale) {
      this.y = 0;
    }

    if (this.x < 0) {
      this.x = canvas.width-scale;
    }

    if (this.y < 0) {
      this.y = canvas.height-scale;
    }
  }

  let d;

  this.changeDirection = function(direction) {

   
    if( direction == 'Left' && d != "RIGHT"){
        this.xSpeed = -scale * 1;
        this.ySpeed = 0;
        d = "LEFT";
        left.play();
    }else if(direction == 'Up' && d != "DOWN"){
        this.xSpeed = 0;
        this.ySpeed = -scale * 1;
        d = "UP";
        up.play();
    }else if(direction == 'Right' && d != "LEFT"){
      this.xSpeed = scale * 1;
        this.ySpeed = 0;
        d = "RIGHT";
        right.play();
    }else if(direction == 'Down' && d != "UP"){
        this.xSpeed = 0;
        this.ySpeed = scale * 1;
        d = "DOWN";
        down.play();
    }
    
  }

  this.eat = function(fruit) {
    if (this.x === fruit.x &&
      this.y === fruit.y) {
      eat.play();
      this.total++;
      return true;
    }

    return false;
  }

  this.checkCollision = function() {
    for (var i=0; i<this.tail.length; i++) {
      if (this.x === this.tail[i].x && this.y === this.tail[i].y) {
        this.total = 0;
        this.tail = [];
        dead.play(); 
        return true;
      }
    }
    return false;
  }


}
