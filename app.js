const path = require("path")
const express = require("express")
const hbs = require("hbs")

const app = express()

const port = process.env.PORT || 3000

const server = require("http").createServer(app)

app.set("view engine", "hbs")
app.set("views", path.join(__dirname, "./templates"))

app.use(express.static(path.join(__dirname, "./public")))


app.get("", (req, res) => {
    res.render("index")
})



console.log(`Port ${port} is available.`)
server.listen(port, () => {
    console.log("Server is up on port " + port + ".")
})